# ocean
Ocean Theme for Brackets

Ocean Theme for Brackets based on <a href="http://chriskempson.com/">Chris Kempson's</a> design of Ocean in the <a href="https://github.com/chriskempson/base16">Base 16 Ocean Color Schemes.</a>
<img src="ocean.png"/>
